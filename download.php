<?php

function ssl_decrypt($source,$type,$key){
// The raw PHP decryption functions appear to work
// on 128 Byte chunks. So this decrypts long text
// encrypted with ssl_encrypt().

$maxlength=128;
$output='';
while($source){
  $input= substr($source,0,$maxlength);
  $source=substr($source,$maxlength);
  if($type=='private'){
    $ok= openssl_private_decrypt($input,$out,$key);
  }else{
    $ok= openssl_public_decrypt($input,$out,$key);
// lets assume you just called an openssl function that failed
  }
       
  $output.=$out;
}
return $output;

}
$rs=ssl_decrypt(file_get_contents('file_storage/'.$_GET['file']),'public',$_GET['key']);
//openssl_public_decrypt(substr(file_get_contents('file_storage/'.$_GET['file']),0,128),$rs,$_GET['key']);
//Set the download headers
header('Content-Description: File Transfer'); 
header('Content-Type: application/octet-stream'); 
header('Content-Length: ' . mb_strlen(file_get_contents('file_storage/'.$_GET['file']), '8bit')); 
header('Content-Disposition: attachment; filename="' . $_GET['file'] . '"'); 
echo $rs;