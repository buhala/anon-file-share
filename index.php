<!doctype html>
<html>
<head>
<title>Anonymous File Upload</title>
</head>
<body>
<center>

<?php

function ssl_encrypt($source,$type,$key){
//Assumes 1024 bit key and encrypts in chunks.

$maxlength=117;
$output='';
while($source){
  $input= substr($source,0,$maxlength);
  $source=substr($source,$maxlength);
  if($type=='private'){
    $ok= openssl_private_encrypt($input,$encrypted,$key);
  }else{
    $ok= openssl_public_encrypt($input,$encrypted,$key);
  }
       
  $output.=$encrypted;
}
return $output;
}
if($_POST){
	$keys=openssl_pkey_new(array(
    "digest_alg" => "sha512",
    "private_key_bits" => 1024,
    "private_key_type" => OPENSSL_KEYTYPE_RSA,
	));
	openssl_pkey_export($keys,$privkey);
	$pubkey=openssl_pkey_get_details($keys);
	$pubkey=$pubkey['key'];//For php older than 5.4
	$filecontent=file_get_contents($_FILES['file']['tmp_name']);
	$encrypted=ssl_encrypt($filecontent,'private',$privkey);
	// lets assume you just called an openssl function that failed
	// lets assume you just called an openssl function that failed

	//echo $encrypted;
	$extension=explode('.',$_FILES['file']['name']);
	$extension=$extension[count($extension)-1];
	$filename=md5(rand()).'.'.$extension;
	//var_dump($encrypted);
	//echo $filename;
	file_put_contents('file_storage/'.$filename,$encrypted);
	touch('file_storage/'.$filename,0,0);
	$url='http://buhala.uchenici.bg/encfiles/download.php?file='.urlencode($filename).'&key='.urlencode($pubkey);
	$curl=curl_init("https://www.googleapis.com/urlshortener/v1/url");
	curl_setopt($curl, CURLOPT_HTTPHEADER, Array('Content-Type: application/json'));
	curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
	curl_setopt($curl,CURLOPT_POST,1);
	curl_setopt($curl,CURLOPT_POSTFIELDS,'{"longUrl": "'.$url.'"}');
	curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
	$curl_result=curl_exec($curl);
	$rs=json_decode($curl_result);
	echo $rs->id;
}
?>
<form method="post" enctype="multipart/form-data">
<table>
<tr><td>File</td><td><input type="file" name="file"></td></tr>
<tr><td colspan="2"><input type="submit" name="act" value="Submit"></td></tr>
</table>
</form>
</center>
</body>
</html>